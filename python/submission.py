#!/usr/bin/env python
# coding: utf-8

from Bio import SeqIO
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix, precision_recall_curve, average_precision_score, auc, plot_confusion_matrix
import matplotlib.pyplot as plt
import numpy as np
from numpy import array
from math import pi, cos, floor
from sklearn.utils import class_weight
from sklearn.preprocessing import StandardScaler


from itertools import combinations, chain

# Keras
import tensorflow as tf
from keras.layers import Conv1D, Dense, Dropout,Flatten, MaxPooling1D, BatchNormalization, Bidirectional, LSTM
from keras.optimizers import SGD
from keras.regularizers import l2
from keras.models import Sequential, load_model
from sklearn.model_selection import train_test_split
from keras.utils.vis_utils import plot_model
from keras.callbacks import EarlyStopping,Callback
from keras import backend
import keras.backend as K

import os

# Paths
datapath = "data"
cnn_models = "CNN_models"
lstm_models = "LSTM-CNN_models"

# Functions
def one_hot_encode(sequences):
    mapping = dict(zip("ACGTN", range(5)))
    seq2 = [mapping[i] for i in sequences]
    return np.eye(5)[seq2]

def load_all_models(n_models, root, prefix):
	all_models = list()
	for i in range(n_models):
		# define filename for this ensemble
		filename = prefix + str(i + 1) + '.h5'
		# load model from file
		model = load_model(os.path.join(root, filename))
		# add to list of members
		all_models.append(model)
		print('>loaded %s' % filename)
	return all_models

def ensemble_predictions(members, testX):
    # make predictions
    yhats = [model.predict(testX) for model in members]
    yhats = array(yhats)
    yhats_reshaped = yhats.reshape(yhats.shape[0], yhats.shape[1])
    # sum across ensemble members
    summed = np.sum(yhats, axis=0)
    results = [x/len(members) for x in summed]
    # argmax across classes
    return yhats_reshaped.T, results

# define function to take input list of models, then calculate precision-recall
def eval_n_models(models, predictions, testY):
    models = [x-1 for x in models]
    predictions_for_eval =  predictions[:,models]
    summed = np.sum(predictions_for_eval, axis = 1)
    mean = [x/len(models) for x in summed]
    pr = average_precision_score(testY, mean)
    return pr

# GPU set-up
gpus = tf.config.list_physical_devices('GPU')

if gpus:
  try:
    tf.config.experimental.set_virtual_device_configuration(
        gpus[0],
        [tf.config.experimental.VirtualDeviceConfiguration(memory_limit=4000)])
    logical_gpus = tf.config.experimental.list_logical_devices('GPU')
    print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs")
  except RuntimeError as e:
    print(e)


# Read data
sequences = []

for seq in SeqIO.parse(os.path.join(datapath, "MDA-MB-231/combined_fasta_ML.fa"), "fasta"):
    sequences.append(seq)

# Encode the data
input_matrix = [one_hot_encode(x) for x in sequences]


features = np.stack(input_matrix)

# Define cut off
outcomes = list(np.zeros(19197)) + list(np.ones(60168))
cutoff = np.sum(outcomes)/len(outcomes)

train_features, test_features, train_labels, test_labels = train_test_split(features,
                                                                            outcomes,
                                                                            test_size=0.20, random_state=42)

train_features, validation_features, train_labels, validation_labels = train_test_split(train_features,
                                                                                       train_labels,
                                                                                       test_size = 0.25, random_state = 12)
# Load CNN models
cnn_members = load_all_models(10, cnn_models, 'cnn_snapshot_')
cnn_members = list(reversed(cnn_members))

# Load LSTM-CNN models
rnn_members = load_all_models(10, lstm_models, 'rnn_snapshot_')
rnn_members = list(reversed(rnn_members))

# Predictions on the testing set
cnn_yhats, cnn_ensemble = ensemble_predictions(cnn_members,
                                                test_features)
rnn_yhats, rnn_ensemble = ensemble_predictions(rnn_members,
                                                test_features)

# Precision-recall curves (Figure 3A)
cnn_precision, cnn_recall, thresholds = precision_recall_curve(test_labels,
                                                                cnn_ensemble)
rnn_precision, rnn_recall, thresholds = precision_recall_curve(test_labels,
                                                                rnn_ensemble)

plt.figure(figsize=(20,8))
plt.title('Precision-recall curve',
            size = 20)

plt.plot(rnn_recall,
        rnn_precision,
        'black',
        linewidth=4)

plt.plot(cnn_recall,
        cnn_precision,
        'blue',
        linewidth=4)
plt.xlabel('Recall',
            size = 20)
plt.ylabel('Precision',
            size = 20)

plt.savefig("Fig3A.png")

# Figure 3B
cnn_classification = [1 if x>cutoff else 0 for x in cnn_ensemble]
cm_ensemble = confusion_matrix(test_labels, cnn_class)
cm_ensemble = cm_ensemble.astype('float') / cm_ensemble.sum(axis = 1)[:, np.newaxis]

plt.figure(figsize=(10,10))
ax = plt.gca()
im = ax.imshow(cm_ensemble, cmap='OrRd')

# annotation of the confusion matrix with percentages
for i in range(2):
    for j in range(2):
        text = ax.text(j, i, str.format('{0:.3f}',
                        cm_ensemble[i, j]),
                        ha="center", va="center",
                        color="black",
                        size = 20)

plt.ylim(1.5, -0.5)
plt.xlim(-0.5,1.5)
plt.xticks(ticks = [0,1],
            labels = ['Smad2', 'Smad3'],
            size = 20)
plt.yticks(ticks= [-0.5,0,0.5,1,1.5],
            labels = ['','Smad2', '','Smad3',''],
            size = 20)
plt.xlabel('Predicted',
            size = 20)
plt.ylabel('Actual',
            size = 20)
plt.title('CNN model',
            size  = 20)

plt.savefig("Fig3B.png")

# Figure 3C
rnn_classification = [1 if x>cutoff else 0 for x in rnn_ensemble]

rnn_cm = confusion_matrix(test_labels,
                            rnn_classification)
rnn_cm = cm.astype('float') / cm.sum(axis = 1)[:, np.newaxis]

plt.figure(figsize=(10,10))
ax = plt.gca()
im = ax.imshow(rnn_cm, cmap='OrRd')


for i in range(2):
    for j in range(2):
        text = ax.text(j, i, str.format('{0:.3f}', cm[i, j]),
                       ha="center", va="center", color="black", size = 20)

plt.ylim(1.5, -0.5)
plt.xlim(-0.5,1.5)

plt.xticks(ticks = [0,1],
            labels = ['Smad2', 'Smad3'],
            size =20)
plt.yticks(ticks= [-0.5,0,0.5,1,1.5],
            labels = ['','Smad2', '','Smad3',""],
            size = 20)

plt.xlabel('Predicted',
            size  = 20)
plt.ylabel('Actual',
            size = 20)
plt.title('CNN-LSTM model',
            size = 20)

plt.savefig('Fig3C.png')

# Figure 3D
input = list(range(1, 11))
combinations = sum([list(map(list,
                combinations(input, i))) for i in range(len(input) + 1)], [])[1:]

scores = []

for i in range(0, len(combinations)):
    scores.append(eval_n_models(combinations[i],
                                rnn_yhats, test_labels))

rnn_aggregates = pd.DataFrame(zip(model_counts, scores),
                            columns=['Models', 'PR'])

plt.figure(figsize=(10,8))
rnn_aggregates.boxplot(by='Models',
                        grid=False,
                        notch = True)

plt.suptitle('')
plt.title('AUPR of CNN-RNN based on ensemble size',
            size = 18)
plt.xlabel('# of models',
            size = 20)
plt.ylabel('')

plt.savefig('Fig3D.png')

# Figure 3E
hesc_smad = []

for seq in SeqIO.parse(os.path.join(datapath, 'validation/smad_hesc.fasta'), 'fasta'):
    hesc_smad.append(seq)

hesc_labels = pd.read_csv(os.path.join(datapath, 'validation/hesc_smad_labels.csv')).x
hesc_smad = np.stack([one_hot_encode(x) for x in hesc_smad])

hesc_yhats, hesc_predictions = ensemble_predictions(rnn_members,
                                                    hesc_smad)
hesc_class = [1 if x>cutoff else 0 for x in hesc_predictions]

hesc_cm = confusion_matrix(hesc_labels,
                            hesc_class)
hesc_cm = hesc_cm.astype('float') / hesc_cm.sum(axis = 1)[:, np.newaxis]

plt.figure(figsize=(10,10))
ax = plt.gca()
im = ax.imshow(hesc_cm, cmap='OrRd')

for i in range(2):
    for j in range(2):
        text = ax.text(j, i, str.format('{0:.3f}',
                        hesc_cm[i, j]),
                        ha="center", va="center",
                        color="black", size = 20)

plt.ylim(1.5, -0.5)
plt.xlim(-0.5,1.5)

plt.xticks(ticks = [0,1],
            labels = ['Smad2', 'Smad3'],
            size = 20)

plt.yticks(ticks= [-0.5,0,0.5,1,1.5],
        labels = ['','Smad2', '','Smad3', ''],
        size = 20)
plt.xlabel('Predicted',
        size = 20)
plt.ylabel('Actual',
        size = 20)
plt.title('CNN-LSTM in hESC',
        size = 20)

plt.savefig('Fig3E.png')

# Model predictions for MCF10A-MII
smad_early = []
smad_late = []

for seq in SeqIO.parse(os.path.join(datapath, 'MCF10A_MII/smad_early.fasta'), 'fasta'):
    smad_early.append(seq)
for seq in SeqIO.parse(os.path.join(datapath, 'MCF10A_MII/smad_late.fasta'), 'fasta'):
    smad_late.append(seq)

# Encode data
smad_early = [one_hot_encode(x) for x in smad_early]
smad_early = np.stack(smad_early)

smad_late = [one_hot_encode(x) for x in smad_late]
smad_late = np.stack(smad_late)


_, smad_early_class = ensemble_predictions(rnn_members,
                                            smad_early)
_, smad_late_class = ensemble_predictions(rnn_members,
                                            smad_late)

smad_early_out = [1 if x>cutoff else 0 for x in smad_early_class]
smad_late_out = [1 if x>cutoff else 0 for x in smad_late_class]


pd.DataFrame(smad_early_out).to_csv(os.path.join(datapath,
                                    'smad_early_predictions.csv'))
pd.DataFrame(smad_late_out).to_csv(os.path.join(datapath,
                                    'smad_late_predictions.csv'))
