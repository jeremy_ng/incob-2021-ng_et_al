# README #

This repository contains all the source codes and models used for the submission Ng et al, 2021 'Deep learning for de-convolution of Smad2 versus Smad3 binding sites'

# Configuration

Models were ran using TensorFlow, on a RTX2060 4GB card equipped with a Ryzen 5 3600 CPU. 

Major software versions used: 
1. Keras==2.4.3
2. Keras-Preprocessing==1.1.2
3. numpy==1.19.5
4. scikit-learn 
5. tensorflow==2.5.0
6. tensorflow-addons==0.12.1
7. tensorflow-datasets==4.2.0
9. tensorflow-estimator==2.5.0
9. tensorflow-gpu==2.5.0

### Who do I talk to? ###
Contact me at jeremy.ng.wk1990@gmail.com for data access. 